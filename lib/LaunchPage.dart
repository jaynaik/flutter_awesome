import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'ViewController/IntroPage.dart';
import 'models/ioHandler.dart';

class Launch extends StatefulWidget {
  @override
  _LaunchState createState() => _LaunchState();
}

class _LaunchState extends State<Launch> {
  final firstLaunchKey = "firstLaunch";
  final handler = IoHandler();

  SharedPreferences prefs;
  @override
  Widget build(BuildContext context) {
    SharedPreferences.getInstance().then((onValue) => {prefs = onValue});
    return _correctLaunchScreen();
  }

  Widget _correctLaunchScreen() {
    if (prefs != null) {
      if (prefs.containsKey(firstLaunchKey)) {
        if (!handler.getBoolFor(firstLaunchKey)) {
          return IntroPage();
        } else {
          prefs.setBool(firstLaunchKey, false);
          return Scaffold(
            appBar: AppBar(
              title: Text(" TITLe"),
            ),
          );
        }
      } else {
        return IntroPage();
      }
    } else {
      return IntroPage();
    }
  }
}
