import 'package:flutter/material.dart';
import 'dart:ui';

class DestinationCard extends StatelessWidget {
  final _assetPath = "lib/Assets/Images/Destinations/";

  final String imagePath;
  final String name;
  DestinationCard({Key key, this.imagePath, this.name}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return _wid(context, name);
  }

  Widget _wid(BuildContext context, String name) {
    print(imagePath);
    return Padding(
        padding: EdgeInsets.symmetric(horizontal: 10.0),
        child: ClipRRect(
            borderRadius: BorderRadius.all(Radius.circular(14.0)),
            child: Stack(
              children: <Widget>[
                Image(
                  height: 200,
                  width: 330,
                  image: AssetImage(_assetPath + imagePath),
                  fit: BoxFit.cover,
                ),
                Positioned(
                    bottom: 25,
                    left: 20,
                    child: Container(
                        decoration: BoxDecoration(
                            color: Color.fromRGBO(128, 128, 128, 0.7),
                            borderRadius:
                                BorderRadius.all(Radius.circular(45.0))),
                        child: ClipRRect(
                            borderRadius:
                                BorderRadius.all(Radius.circular(45.0)),
                            child: BackdropFilter(
                              filter: ImageFilter.blur(sigmaX: 20, sigmaY: 20),
                              child: Text("  $name  ",
                                  style: TextStyle(
                                      fontSize: 19,
                                      color: Colors.white,
                                      fontWeight: FontWeight.bold)),
                            ))))
              ],
            )));
  }
}
