import 'package:flutter/material.dart';

class FlightCard extends StatelessWidget {
  final String terminalFrom;
  final String terminalTo;

  final String airportCodeFrom;
  final String airportCodeTo;

  final String timeFrom;
  final String timeTo;

  final String cityFrom;
  final String cityTo;

  final String flightNumber;
  final String airlineImage;

  final assetPath = "lib/Assets/Images/airlines/";

  FlightCard(
      {Key key,
      this.terminalFrom,
      this.terminalTo,
      this.airportCodeFrom,
      this.airportCodeTo,
      this.cityFrom,
      this.cityTo,
      this.timeFrom,
      this.timeTo,
      this.flightNumber,
      this.airlineImage})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    var width =
        MediaQuery.of(context).size.width < MediaQuery.of(context).size.height
            ? MediaQuery.of(context).size.width
            : MediaQuery.of(context).size.height;
    print(width);
    return Container(
      // width: width - (16 * 3),  // removing margin
      //padding: EdgeInsets.only(left: 15, right: 15),
      height: 75,
      decoration: BoxDecoration(
          color: Colors.white,
          borderRadius: BorderRadius.all(Radius.circular(15)),
          boxShadow: [
            BoxShadow(
                offset: Offset(5, 5), color: Colors.black26, blurRadius: 5)
          ]),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
        children: <Widget>[
          Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              Text(airportCodeFrom,
                  style: TextStyle(fontSize: 18, fontWeight: FontWeight.bold)),
              Text(cityFrom,
                  style: TextStyle(fontSize: 10, color: Colors.black45))
            ],
          ),
          Column(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: <Widget>[
                Icon(
                  Icons.flight_takeoff,
                  color: Colors.black45,
                  size: 30,
                ),
                Text(timeFrom,
                    style: TextStyle(color: Colors.black, fontSize: 14))
              ]),
          Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              Text("Terminal",
                  style: TextStyle(fontSize: 10, color: Colors.black45)),
              Text(terminalFrom,
                  style: TextStyle(fontSize: 18, fontWeight: FontWeight.bold)),
            ],
          ),
          Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                Image(
                  image: AssetImage(assetPath + airlineImage),
                  height: 22,
                  width: 22,
                  fit: BoxFit.cover,
                ),
                RotationTransition(
                  turns: AlwaysStoppedAnimation(90 / 360),
                  child: Icon(
                    Icons.airplanemode_active,
                    color: Color.fromRGBO(82, 107, 255, 1.0),
                  ),
                ),
                Text(flightNumber)
              ]),
          Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              Text("Terminal",
                  style: TextStyle(fontSize: 10, color: Colors.black45)),
              Text(terminalTo,
                  style: TextStyle(fontSize: 18, fontWeight: FontWeight.bold)),
            ],
          ),
          Column(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: <Widget>[
                Icon(Icons.flight_land, color: Colors.black45, size: 30),
                Text(timeTo,
                    style: TextStyle(color: Colors.black, fontSize: 14))
              ]),
          Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              Text(airportCodeTo,
                  style: TextStyle(fontSize: 18, fontWeight: FontWeight.bold)),
              Text(cityTo,
                  style: TextStyle(fontSize: 10, color: Colors.black45))
            ],
          ),
        ],
      ),
    );
  }
}
