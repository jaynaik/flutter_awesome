import 'package:flutter/material.dart';
import 'dart:ui';
class RecommendedCard extends StatelessWidget {
  final _assetPath = "lib/Assets/Images/Recommended/";

  final String imagePath ;
  final String name;

  RecommendedCard({Key key, this.imagePath, this.name}) : super(key: key);


  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.only(left: 15,right:  15,top: 7, bottom: 7),
//      EdgeInsets.symmetric(horizontal: 15),

      child: Container(
//          height: 220,
//          width: 112,
          decoration: BoxDecoration(color: Colors.white,borderRadius: BorderRadius.all(Radius.circular(15)),boxShadow: [BoxShadow(offset: Offset(1, 1),color: Colors.black12,blurRadius: 2,spreadRadius: 2)]),
        child: ClipRRect(
      borderRadius: BorderRadius.all(Radius.circular(14.0)),
      child: Stack(children: <Widget>[
        Image(height: 200, width: 128,  image: AssetImage(_assetPath + imagePath),fit: BoxFit.cover),
        Positioned(bottom: 12,left: 8,child: Container(decoration: BoxDecoration(color: Color.fromRGBO(200, 200, 200, .6), borderRadius: BorderRadius.all(Radius.circular(45.0))),
        child: ClipRRect(borderRadius: BorderRadius.all(Radius.circular(45.0)),
          child: BackdropFilter(filter:  ImageFilter.blur(sigmaX: 10,sigmaY: 10),
              child:
             // Padding(padding: EdgeInsets.symmetric(horizontal: 15),             child:
              Row( mainAxisAlignment: MainAxisAlignment.center  ,children: <Widget>[
              Text("  $name  ",style: TextStyle(fontSize: 18,color: Colors.white, ),) ])  ) //)
          ,)
        ),)


      ],),
      )
    )


      ,
    );
  }
}
