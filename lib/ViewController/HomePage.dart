import 'package:flutter/material.dart';
import '../View/destinationCard.dart';
import '../View/recommendedCard.dart';
import '../View/flightCard.dart';
import 'package:image_picker/image_picker.dart';
import 'dart:io';

enum imagePickMode {
  camera,
  gallery
}

class HomePage extends StatefulWidget {
  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  File _profilePictureFile;
  final GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();
  double _viewport = 0.8;

  @override
  Widget build(BuildContext context) {

    if(MediaQuery.of(context).size.width > 600) {_viewport = 0.4;} else {_viewport = 0.8; }

    return Scaffold(key: _scaffoldKey,
      body: Container(
        padding: EdgeInsets.only(top: 10),
        height: MediaQuery.of(context).size.height - 44,
        width: MediaQuery.of(context).size.width,
        child: ListView(shrinkWrap: true, children: <Widget>[
          Column(
            mainAxisAlignment: MainAxisAlignment.start,
            children: <Widget>[
              _titleRect(context),
              _destinationScroll(context),
              _recommended(context),
              _bookings(context)
            ],
          )
        ]),
      ),
    );
  }

  Widget _titleRect(BuildContext ctx) {
    return Padding(
      padding: EdgeInsets.only(right: 16, left: 16),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: <Widget>[
          Text(
            _hiString(),
            style: TextStyle(
                fontSize: 22, fontWeight: FontWeight.bold, color: Colors.black),
          ),
          CircleAvatar(
            radius: 15,
            backgroundColor: Colors.white,
            child: GestureDetector(
              onTap: ()=> _showModal(context),
              child: Container(
                child: ClipRRect(
                  borderRadius: BorderRadius.all(Radius.circular(15)),
                  child: _profilePic(),
                ),
              ),
            ),
          )
        ],
      ),
    );
  }

  var _hi_user = "Hi, ";
  final _destinationImages = [
    "Bali.jpg",
    "venice.jpg",
    "london.jpg",
    "greece.jpg"
  ];
  final _destinationNames = ["Bali", "Venice", "London", "Greece"];
  String _hiString() {
    return _hi_user + "Alexa" + "\nWhere do you want to go?";
  }

  _destinationViewAllPressed(BuildContext ctx) {
    print("show all destination");
  }

  Widget _destinationScroll(BuildContext context) {
    return Column(
      mainAxisSize: MainAxisSize.min,
      children: <Widget>[
        Padding(
            padding: EdgeInsets.only(left: 16, top: 10, right: 16),
            child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  Text(
                    "Top Destinations",
                    style: TextStyle(
                        fontWeight: FontWeight.bold,
                        color: Colors.black,
                        fontSize: 20),
                  ),
                  FlatButton(
                      highlightColor: Colors.transparent,
                      splashColor: Colors.transparent,
                      onPressed: () => _destinationViewAllPressed(context),
                      child: Text(
                        "View All",
                        style: TextStyle(
                            fontSize: 16,
                            fontWeight: FontWeight.bold,
                            color: Color.fromRGBO(82, 107, 255, 1.0)),
                      ))
                ])),
        SizedBox(
          // you may want to use an aspect ratio here for tablet support
          height: 200.0,

          child: PageView.builder(
            itemCount: _destinationImages.length,
            // store this controller in a State to save the carousel scroll position
            controller: PageController(viewportFraction: _viewport),
            itemBuilder: (BuildContext context, int itemIndex) {
              return DestinationCard(
                imagePath: _destinationImages[itemIndex],
                name: _destinationNames[itemIndex],
              );
            },
          ),
        )
      ],
    );
  }

  /*  ##################################### Recommended  ########################################  */

  final _recommendedNames = ["Paris", "Rome", "Los Angeles", "Paris", "Paris"];
  final _recommendedImages = [
    "paris.jpeg",
    "rome.jpg",
    "losangeles.jpeg",
    "paris.jpeg",
    "paris.jpeg"
  ];

  Widget _recommended(BuildContext context) {
    return Column(
      mainAxisAlignment: MainAxisAlignment.center,
      children: <Widget>[
        Padding(
            padding: EdgeInsets.only(left: 16, top: 15, right: 16, bottom: 15),
            child: Row(
                mainAxisAlignment: MainAxisAlignment.start,
                children: <Widget>[
                  Text(
                    "Recommended",
                    style: TextStyle(
                        fontWeight: FontWeight.bold,
                        color: Colors.black,
                        fontSize: 20),
                  )
                ])),
        SizedBox(
            height: 200,
            child: ListView.builder(
              scrollDirection: Axis.horizontal,
              itemCount: _recommendedNames.length,
              itemBuilder: (BuildContext context, int itemIndex) {
                return RecommendedCard(
                  imagePath: _recommendedImages[itemIndex],
                  name: _recommendedNames[itemIndex],
                );
              },
            )),
      ],
    );
  }

  final _airportFrom = ["SFO", "DBX"];
  final _airportTo = ["JFK", "LAX"];

  final _terminalFrom = ["C1", "D2"];
  final _terminalTo = ["D2", "A1"];

  final _timeFrom = ["23:45", "08:15"];
  final _timeTo = ["16:00", "22:20"];

  final _cityFrom = ["San Francisco, CA", "Los Angeles, CA"];
  final _cityTo = ["New York, NY", "Dubai, UAE"];

  final _flightNumber = ["B618", "EK039"];
  final _flightImage = ["jetblue.png", "emirates.png"];

  Widget _bookings(BuildContext context) {
    return Column(
      mainAxisAlignment: MainAxisAlignment.start,
      children: <Widget>[
        Padding(
            padding: EdgeInsets.only(left: 16, top: 15, right: 16, bottom: 15),
            child: Row(
                mainAxisAlignment: MainAxisAlignment.start,
                children: <Widget>[
                  Text(
                    "Your Bookings",
                    style: TextStyle(
                        fontWeight: FontWeight.bold,
                        color: Colors.black,
                        fontSize: 20),
                  )
                ])),
        SizedBox(
            height: 200,
            width: MediaQuery.of(context).size.width - (16 * 2) < 400 ? MediaQuery.of(context).size.width - (16 * 2) : 600 ,
            child: ListView.separated(
                scrollDirection: Axis.vertical,
                itemCount: _terminalFrom.length,
                separatorBuilder: (ctx, index) {
                  return Divider();
                },
                itemBuilder: (BuildContext, int itemIndex) {
                  return FlightCard(
                    airportCodeFrom: _airportFrom[itemIndex],
                    airportCodeTo: _airportTo[itemIndex],
                    cityFrom: _cityFrom[itemIndex],
                    cityTo: _cityTo[itemIndex],
                    terminalFrom: _terminalFrom[itemIndex],
                    terminalTo: _terminalTo[itemIndex],
                    timeFrom: _timeFrom[itemIndex],
                    timeTo: _timeTo[itemIndex],
                    airlineImage: _flightImage[itemIndex],
                    flightNumber: _flightNumber[itemIndex],
                  );
                }))
      ],
    );
  }

  void _showModal(BuildContext context) {
    showModalBottomSheet(context: context, builder: (BuildContext ctx) {return _imagePickerOptionModal(); });
  }

  void _imagePicker(imagePickMode mode) async {
    Navigator.of(_scaffoldKey.currentState.context).pop();
    switch(mode){
      case imagePickMode.camera: {
        File image = await ImagePicker.pickImage(source: ImageSource.camera);
        setState(() {
          _profilePictureFile = image;
        });
        break;
      }
      case imagePickMode.gallery: {
        File image = await ImagePicker.pickImage(source: ImageSource.gallery);
        setState(() {
          _profilePictureFile = image;
        });
        break;
      }
    }



  }

  Image _profilePic() {
    if (_profilePictureFile == null) {
      return Image(
          width: 30,
          height: 30,
          image: AssetImage("lib/Assets/Images/profile-image.jpg"),
          fit: BoxFit.cover);
    } else {
      return Image.file(
        _profilePictureFile,
        width: 30,
        height: 30,
        fit: BoxFit.cover,
      );
    }
  }

  Widget _imagePickerOptionModal() {
    return Container(
      child: SafeArea(  bottom: true,
        child:Wrap(
        children: <Widget>[
          ListTile(
            leading: Icon(Icons.camera),
            title: Text("Camera"),
            onTap: ()=> _imagePicker(imagePickMode.camera) ,
          ),
          ListTile(
            leading: Icon(Icons.photo_library),
            title: Text("Photo gallery"),
            onTap: ()=> _imagePicker(imagePickMode.gallery),
          )
        ],
      ),
       ),

    );
  }
}
