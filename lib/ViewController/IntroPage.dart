import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';
// import 'package:page_view_indicator/page_view_indicator.dart';
import 'package:dots_indicator/dots_indicator.dart';
  

class IntroPage extends StatefulWidget {
  @override
  _IntroPageState createState() => _IntroPageState();
}

class _IntroPageState extends State<IntroPage> {
  @override
  Widget build(BuildContext context) {
    mQuery = MediaQuery.of(context);
    //pageChangeNotifier.addListener(_pageChanged);
    return Scaffold(backgroundColor: Colors.black,
        body: Stack( 
      children: <Widget>[

        Container(child: PageView(onPageChanged:_pageChanged,controller: _pageController,children: <Widget>[

          Container(decoration: BoxDecoration(color: Colors.black),child: Column(children: <Widget>[
            Container(width: MediaQuery.of(context).size.width, height: MediaQuery.of(context).size.height / 2 ,child: Stack(children: <Widget>[
              Positioned(left: 25,top: 70,child: Text("Browse",style: TextStyle(fontSize: 64,color: Colors.white,fontWeight: FontWeight.bold, ),),),
              Positioned(left: 25, top: 180, width: MediaQuery.of(context).size.width - 80,child: Text(_loremIpsum(),style: TextStyle(color: Colors.white,fontSize: 14),),),   
              Positioned(right: 28, top: 35, child: FlatButton(onPressed : _skipToLast,child: Text("Skip",style: TextStyle(color: Colors.white,),),),)      
            ],), ), 
            Container(width: MediaQuery.of(context).size.width, height: MediaQuery.of(context).size.height / 2 ,child: Image(fit: BoxFit.cover,image: AssetImage(_pathToAssets+"intro-1.png"),),)],),),


          Container(decoration: BoxDecoration(color: Colors.black),child: Column(children: <Widget>[
            Container(width: MediaQuery.of(context).size.width, height: MediaQuery.of(context).size.height / 2 ,child: Stack(children: <Widget>[
              Positioned(left: 25,top: 70,  child: Text("Order",style: TextStyle(fontSize: 64,color: Colors.white,fontWeight: FontWeight.bold, ),),),
              Positioned(left: 25, top: 180, width: MediaQuery.of(context).size.width - 80,child: Text(_loremIpsum(),style: TextStyle(color: Colors.white,fontSize: 14),),),  
              Positioned(right: 28, top: 35, child: FlatButton(onPressed : _skipToLast,child: Text("Skip",style: TextStyle(color: Colors.white,),),),)     
            ],), ), 
            Container(width: MediaQuery.of(context).size.width, height: MediaQuery.of(context).size.height / 2 ,child: Image(fit: BoxFit.cover,image: AssetImage(_pathToAssets+"intro-2.png"),),)],),),


          Container(decoration: BoxDecoration(color: Colors.black),child: Column(children: <Widget>[
            Container(width: MediaQuery.of(context).size.width, height: MediaQuery.of(context).size.height / 2 ,child: Stack(children: <Widget>[
              
              Positioned(left: 25,top: 70, child: Text("Enjoy",style: TextStyle(fontSize: 64,color: Colors.white,fontWeight: FontWeight.bold, ),),),
              Positioned(left: 25, top: 180, width: MediaQuery.of(context).size.width - 80,child: Text(_loremIpsum(),style: TextStyle(color: Colors.white,fontSize: 14),),),    
              Positioned(right: 0,top: MediaQuery.of(context).size.height /2 - 90,child:Container(decoration: BoxDecoration(color: Colors.white),height: 44, width: 160, child:  Row(mainAxisAlignment: MainAxisAlignment.center,children: <Widget>[GestureDetector(onTap: ()=>_getStartedPressed(context),child: Text("Get Started ",style: TextStyle(color: Colors.black,fontWeight: FontWeight.bold))) ,Icon(Icons.arrow_forward)],)   , ),),
              
            ],), ), 
            Container(width: MediaQuery.of(context).size.width, height: MediaQuery.of(context).size.height / 2 ,child: Image(fit: BoxFit.cover,image: AssetImage(_pathToAssets+"intro-3.png"),),)],),),
          
          
          ],),),
          Positioned(
            bottom: (MediaQuery.of(context).size.height / 2) + 44,
            height: 44.0,
            left: 12.0,
            width: MediaQuery.of(context).size.width,
            child: Row(
                mainAxisAlignment: MainAxisAlignment.start,
                children: <Widget>[ 
                 DotsIndicator(numberOfDot: 3,position: _currentPage,dotActiveColor: Colors.white,) 
                ],),),
        
        ],),);
  }

  // ValueNotifier<int> pageChangeNotifier = ValueNotifier(0);
  int _currentPage=0;
  final _pathToAssets = "lib/Assets/Images/";
  MediaQueryData mQuery;
  final PageController _pageController = PageController();

  

  void _pageChanged(int toPage) {
    setState(() {
      _currentPage = toPage;
    });
    //print("page changed");
  }

  void _skipToLast() {
    _pageController.jumpToPage(2);
  }

  void _getStartedPressed(BuildContext ctx) {
    print("get started pressed");
    // replace router here
    Navigator.of(context).pushReplacementNamed("/SignupPage");
  }


  String _loremIpsum() {
return "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.";
  }
}
