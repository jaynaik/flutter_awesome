import 'package:flutter/material.dart';
import '../models/BottomWaveClipper.dart';
import '../models/RegExCheck.dart';

class LoginPage extends StatefulWidget {
  @override
  _LoginPageState createState() => _LoginPageState();
}

class _LoginPageState extends State<LoginPage> {
  final passwordFieldController = TextEditingController();

  final emailFieldController = TextEditingController();

  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();
  
  final _inputFieldBorderRadius = BorderRadius.all(Radius.circular(15));
  final _inputFieldFocusedBorderColor = Color.fromRGBO(50, 30, 114, 1.0);
  final _inputFieldEnabledBorderColor = Color.fromRGBO(67, 186, 196, 1.0);

  final _gradientColor = [    Color.fromRGBO(102, 126, 238, 1.0)  ,    Color.fromRGBO(64, 64, 238, 1.0)  ];

  bool _autoValidate = false;

// final _assetPath = "lib/Assets/Images/";

  @override
  Widget build(BuildContext context) {
    
    print(MediaQuery.of(context).size.width);
    return Scaffold(
      body: Container(
        width: MediaQuery.of(context).size.width,
        height: MediaQuery.of(context).size.height,
        decoration: BoxDecoration(color: Colors.white),
        child: Stack(
          children: <Widget>[
            Positioned(
              top: 0,
              left: 0,
              width: MediaQuery.of(context).size.width,
              child: ClipPath(
                clipper: BottomWaveClipper(),
                child: Container(
                    decoration: BoxDecoration(
                        gradient: LinearGradient(stops: [
                      0.0,
                      1.0
                    ], colors: [
                      Color.fromRGBO(87, 109, 233, 1.0),
                      Color.fromRGBO(44, 41, 232, 1.0)
                    ])),
                    height: 280),
              ),
            ),
            Positioned(
              top: 80,
              left: 16,
              height: 112,
              child: Column(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Text("Welcome back,",
                      style: TextStyle(
                          color: Colors.white,
                          fontWeight: FontWeight.bold,
                          fontSize: 23)),
                  Text("Log In!",
                      style: TextStyle(
                          color: Colors.white,
                          fontWeight: FontWeight.bold,
                          fontSize: 52))
                ],
              ),
            ),
            Positioned(
              left: 25,
              bottom: 44,
              height: 360,
              width: 370,
              child:
              Form(autovalidate: _autoValidate,
              key: _formKey,
              child:
              Column(
                crossAxisAlignment: CrossAxisAlignment.center,
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: <Widget>[
                  TextFormField(
                autocorrect: false,
                validator: RegExCheck.email,
                    controller: emailFieldController,
                    decoration: InputDecoration(
                        labelText: "EMAIL ADDRESS",
                        labelStyle: TextStyle(),
                        focusedBorder: OutlineInputBorder(
                            borderSide: BorderSide(
                                color: _inputFieldFocusedBorderColor,
                                style: BorderStyle.solid),
                            borderRadius:
                            _inputFieldBorderRadius),
                        enabledBorder: OutlineInputBorder(
                            borderSide: BorderSide(
                                color: _inputFieldEnabledBorderColor,
                                style: BorderStyle.solid),
                            borderRadius:
                                _inputFieldBorderRadius),
                        errorBorder: OutlineInputBorder(
                            borderSide: BorderSide(
                                color: Colors.red,
                                style: BorderStyle.solid),
                            borderRadius:
                            _inputFieldBorderRadius),
                      focusedErrorBorder: OutlineInputBorder(
                          borderSide: BorderSide(
                              color: Colors.redAccent,
                              style: BorderStyle.solid),
                          borderRadius:
                          _inputFieldBorderRadius)

                    ),

                  ),
                  TextFormField(
                    autocorrect: false,
                    validator: RegExCheck.password,
                    controller: passwordFieldController,
                    obscureText: true,
                    decoration: InputDecoration(
                        labelText: "PASSWORD",
                        labelStyle: TextStyle(),
                        focusedBorder: OutlineInputBorder(
                            borderSide: BorderSide(
                                color: _inputFieldFocusedBorderColor,
                                style: BorderStyle.solid),
                            borderRadius:
                                _inputFieldBorderRadius),
                        enabledBorder: OutlineInputBorder(
                            borderSide: BorderSide(
                                color: _inputFieldEnabledBorderColor,
                                style: BorderStyle.solid),
                            borderRadius:
                                _inputFieldBorderRadius),
                        errorBorder: OutlineInputBorder(
                            borderSide: BorderSide(
                                color: Colors.red,
                                style: BorderStyle.solid),
                            borderRadius:
                            _inputFieldBorderRadius),
                        focusedErrorBorder: OutlineInputBorder(
                            borderSide: BorderSide(
                                color: Colors.redAccent,
                                style: BorderStyle.solid),
                            borderRadius:
                            _inputFieldBorderRadius)

                    ),
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: <Widget>[
                      Row(children: <Widget>[
                        Switch(
                          inactiveTrackColor:
                              Color.fromRGBO(186, 186, 186, 1.0),
                          inactiveThumbColor: Color.fromRGBO(59, 59, 59, 1.0),
                          activeTrackColor: Color.fromRGBO(186, 186, 186, 1.0),
                          activeColor: Color.fromRGBO(79, 100, 237, 1.0),
                          value: _switchBool,
                          onChanged: _switchChanged,
                        ),
                        Text(
                          "Remember me",
                          style: TextStyle(
                              color: Color.fromRGBO(166, 166, 166, 1.0)),
                        ),
                      ]),
                      FlatButton(
                        onPressed: () => {print("forgot")},
                        child: Text("Forgot password?",
                            style: TextStyle(
                                color: Color.fromRGBO(166, 166, 166, 1.0))),
                        highlightColor: Colors.transparent,
                        splashColor: Colors.transparent,
                      )
                    ],
                  ),
                  Padding(
                    padding: EdgeInsets.only(top: 40, bottom: 30),
                    child: GestureDetector(
                      onTap: () => _login(context),
                      child: Container(
                        height: 52,
                        width: 240,
                        child: Center(
                            child: Text("Log In",
                                style: TextStyle(
                                    color: Colors.white,
                                    fontWeight: FontWeight.bold,
                                    fontSize: 20))),
                        decoration: BoxDecoration(
                            boxShadow: [
                              BoxShadow(
                                  color: Color.fromRGBO(193, 195, 255, 1.0),
                                  offset: Offset(0, 10),
                                  blurRadius: 10)
                            ],
                            borderRadius: BorderRadius.all(Radius.circular(30)),
                            gradient: LinearGradient(stops: [
                              0.0,
                              1.0
                            ], colors: _gradientColor)),
                      ),
                    ),
                  ),
                ],
              ),
    )
            ),
          ],
        ),
      ),
    );
  }


  bool _switchBool = false;
  void _switchChanged(bool to) {
    setState(() {
      _switchBool = to;
    });
  }

  void _login(BuildContext context) {
    setState(() {
      _autoValidate = true;
    });

    if(_formKey.currentState.validate()) {
      if(_authenticate(emailFieldController.text, passwordFieldController.text))
        Navigator.of(context).pushNamedAndRemoveUntil('/HomePage', (Route<dynamic> route) => false);
    }
  }

  bool _authenticate(String email, String pass){
    //if(email == "tomriddle@yopmail.com" && pass == "Abcd1234")
        return true;
    //return false;
  }
}
