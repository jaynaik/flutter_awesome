import 'package:flutter/material.dart';
import '../models/BottomWaveClipper.dart';
import '../models/RegExCheck.dart';

class SignupPage extends StatefulWidget {
  @override
  _SignupPageState createState() => _SignupPageState();
}

class _SignupPageState extends State<SignupPage> {
  final usernamefieldController = TextEditingController();
  final passwordfieldController = TextEditingController();
  final emailfieldController = TextEditingController();
  final _assetPath = "lib/Assets/Images/";

  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();
  final GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();

  final _inputFieldBorderRadius = BorderRadius.all(Radius.circular(15));
  final _inputFieldFocusedBorderColor = Color.fromRGBO(50, 30, 114, 1.0);
  final _inputFieldEnabledBorderColor = Color.fromRGBO(67, 186, 196, 1.0);

  final _gradientColor = [    Color.fromRGBO(102, 126, 238, 1.0)  ,    Color.fromRGBO(64, 64, 238, 1.0)  ];
  bool _autoValidate = false;

  @override
  Widget build(BuildContext context) {
    return Scaffold(key: _scaffoldKey,
      body: Container(
        width: MediaQuery.of(context).size.width,
        height: MediaQuery.of(context).size.height,
        decoration: BoxDecoration(color: Colors.white),
        child: Stack(children: <Widget>[
          Positioned(
            top: 0,
            left: 0,
            width: MediaQuery.of(context).size.width,
            child: ClipPath(
              clipper: BottomWaveClipper(),
              child: Container(
                  decoration: BoxDecoration(
                      gradient: LinearGradient(stops: [
                    0.0,
                    1.0
                  ], colors: _gradientColor
//                      [
//                    Color.fromRGBO(87, 109, 233, 1.0),
//                    Color.fromRGBO(44, 41, 232, 1.0)
//                  ]
                      )),
                  height: 240),
            ),
          ),
          Positioned(
            top: 80,
            left: 16,
            height: 90,
            child: Column(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Text("Hello,",
                    style: TextStyle(
                        color: Colors.white,
                        fontWeight: FontWeight.bold,
                        fontSize: 23)),
                Text("Sign Up!",
                    style: TextStyle(
                        color: Colors.white,
                        fontWeight: FontWeight.bold,
                        fontSize: 52))
              ],
            ),
          ),
          Positioned(
              left: 25,
              bottom: 0,
              height: 500,
              width: 370,
              child:
              Form( key: _formKey,autovalidate: _autoValidate,
                child:
              Column(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: <Widget>[
                  TextFormField(autocorrect:  false,validator: RegExCheck.username,
                    controller: usernamefieldController,
                    decoration: InputDecoration(
                        labelText: "USER NAME",
                        labelStyle: TextStyle(),
                        focusedBorder: OutlineInputBorder(
                            borderSide: BorderSide(
                                color: _inputFieldFocusedBorderColor,
                                style: BorderStyle.solid),
                            borderRadius:
                            _inputFieldBorderRadius),
                        enabledBorder: OutlineInputBorder(
                            borderSide: BorderSide(
                                color: _inputFieldEnabledBorderColor,
                                style: BorderStyle.solid),
                            borderRadius:
                            _inputFieldBorderRadius),
                        errorBorder: OutlineInputBorder(
                    borderSide: BorderSide(
                    color: Colors.red,
                        style: BorderStyle.solid),
                  borderRadius:
                  _inputFieldBorderRadius),
                  focusedErrorBorder: OutlineInputBorder(
                      borderSide: BorderSide(
                          color: Colors.redAccent,
                          style: BorderStyle.solid),
                      borderRadius:
                      _inputFieldBorderRadius)

                    ),

                  ),
                  TextFormField(autocorrect: false,validator: RegExCheck.email,
                    controller: emailfieldController,
                    decoration: InputDecoration(
                        labelText: "EMAIL ADDERSS",
                        labelStyle: TextStyle(),
                        focusedBorder: OutlineInputBorder(
                            borderSide: BorderSide(
                                color: _inputFieldFocusedBorderColor,
                                style: BorderStyle.solid),
                            borderRadius:
                            _inputFieldBorderRadius),
                        enabledBorder: OutlineInputBorder(
                            borderSide: BorderSide(
                                color: _inputFieldEnabledBorderColor,
                                style: BorderStyle.solid),
                            borderRadius:
                            _inputFieldBorderRadius),
                        errorBorder: OutlineInputBorder(
                            borderSide: BorderSide(
                                color: Colors.red,
                                style: BorderStyle.solid),
                            borderRadius:
                            _inputFieldBorderRadius),
                        focusedErrorBorder: OutlineInputBorder(
                            borderSide: BorderSide(
                                color: Colors.redAccent,
                                style: BorderStyle.solid),
                            borderRadius:
                            _inputFieldBorderRadius)
                    ),
                  ),
                  TextFormField(autocorrect: false,validator: RegExCheck.password,
                    obscureText: true,
                    controller: passwordfieldController,
                    decoration: InputDecoration(
                        labelText: "PASSWORD",
                        labelStyle: TextStyle(),
                        focusedBorder: OutlineInputBorder(
                            borderSide: BorderSide(
                                color: _inputFieldFocusedBorderColor,
                                style: BorderStyle.solid),
                            borderRadius:
                            _inputFieldBorderRadius),
                        enabledBorder: OutlineInputBorder(
                            borderSide: BorderSide(
                                color: _inputFieldEnabledBorderColor,
                                style: BorderStyle.solid),
                            borderRadius:
                            _inputFieldBorderRadius),
                        errorBorder: OutlineInputBorder(
                            borderSide: BorderSide(
                                color: Colors.red,
                                style: BorderStyle.solid),
                            borderRadius:
                            _inputFieldBorderRadius),
                        focusedErrorBorder: OutlineInputBorder(
                            borderSide: BorderSide(
                                color: Colors.redAccent,
                                style: BorderStyle.solid),
                            borderRadius:
                            _inputFieldBorderRadius)
                    ),
                  ),
                  Row(
                    children: <Widget>[
                      Switch(
                        inactiveTrackColor: Color.fromRGBO(186, 186, 186, 1.0),
                        inactiveThumbColor: Color.fromRGBO(59, 59, 59, 1.0),
                        activeTrackColor: Color.fromRGBO(186, 186, 186, 1.0),
                        activeColor: Color.fromRGBO(79, 100, 237, 1.0),
                        value: _switchBool,
                        onChanged: _switchChanged,
                      ),
                      Text(
                        "I accept the terms & policy",
                        style: TextStyle(
                            color: Color.fromRGBO(166, 166, 166, 1.0),
                            fontSize: 17),
                      )
                    ],
                  ), GestureDetector(child:
                  Container(
                    height: 52,
                    width: 240,
                    child: Center(
                        child: Text("Sign Up",
                            style: TextStyle(
                                color: Colors.white,
                                fontWeight: FontWeight.bold,
                                fontSize: 20)))  ,
                    decoration: BoxDecoration(
                        boxShadow: [
                          BoxShadow(
                              color: Color.fromRGBO(193, 195, 255, 1.0),
                              offset: Offset(0, 10),
                              blurRadius: 10)
                        ],
                        borderRadius: BorderRadius.all(Radius.circular(30)),
                        gradient: LinearGradient(stops: [
                          0.0,
                          1.0
                        ], colors: _gradientColor)),
                  ),onTap: _signupPressed,)  ,
                  Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      Text("Already have an account? "),
                      FlatButton(
                        child: Text(
                          "Login",
                          style:
                              TextStyle(decoration: TextDecoration.underline),
                        ),
                        onPressed: () => _goToLogin(context),
                      )
                    ],
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      Container(
                        padding: EdgeInsets.only(left: 7, right: 7),
                        child: CircleAvatar(
                          radius: 16,
                          child: Container(
                              decoration: BoxDecoration(
                                  borderRadius: BorderRadius.circular(5)),
                              child: Image(
                                image: AssetImage(
                                    _assetPath + "share-twitter.png"),
                              )),
                        ),
                      ),
                      Container(
                        padding: EdgeInsets.only(left: 7, right: 7),
                        child: CircleAvatar(
                          radius: 16,
                          child: Container(
                              decoration: BoxDecoration(
                                  borderRadius: BorderRadius.circular(5)),
                              child: Image(
                                image:
                                    AssetImage(_assetPath + "share-google.png"),
                              )),
                        ),
                      ),
                      Container(
                        padding: EdgeInsets.only(left: 7, right: 7),
                        child: CircleAvatar(
                          radius: 16,
                          child: Container(
                              decoration: BoxDecoration(
                                  borderRadius: BorderRadius.circular(5)),
                              child: Image(
                                image: AssetImage(_assetPath + "share-fb.png"),
                              )),
                        ),
                      )
                    ],
                  )
                ],
              )

          )
          )
        ]),
      ),
    );
  }

void _signupPressed(){
    setState(() {
      _autoValidate = true;
    });
    if(_formKey.currentState.validate()){
      if(!_switchBool){
        _scaffoldKey.currentState..showSnackBar(new SnackBar(content: Text("Please accept terms and policy"), duration: Duration(seconds: 1, milliseconds: 500), ));
      } else {
        _goToLogin(context);
      }

    }
}

  bool _switchBool = false;
  void _switchChanged(bool to) {
    setState(() {
      _switchBool = to;
    });
  }

  void _goToLogin(BuildContext context) {
    Navigator.of(context).pushNamed("/LoginPage");
  }
}
