import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:flutter/services.dart';
import 'LaunchPage.dart';
import 'ViewController/LoginPage.dart';
import 'ViewController/SignupPage.dart';
import 'ViewController/HomePage.dart';

void main()  {
  // debugPaintSizeEnabled = true;
  // debugPaintBaselinesEnabled = true;
  // debugPaintPointersEnabled = true;
  SystemChrome.setPreferredOrientations([DeviceOrientation.portraitUp])
      .then((_) {
    runApp(new MyApp());
  });
  //runApp(MyApp());
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
       debugShowCheckedModeBanner: false,
      title: 'Flutter Demo',
      routes: <String, WidgetBuilder> {
        '/introPage': (BuildContext context) => new Launch(),
        '/LoginPage': (BuildContext context) => new LoginPage(),
        '/SignupPage': (BuildContext context) => new SignupPage(),
        '/HomePage': (BuildContext context) => new HomePage()
      },
      theme: ThemeData(
         
        primarySwatch: Colors.blue,
      ),
      home: Launch(),
    );
  }
}
