
class RegExCheck {
  static final usernameRegex = RegExp("^\\s*(?:\\S\\s*){6,14}\$");
  static final emailChecker = RegExp("^([\\w\\-\\.]+)@((\\[([0-9]{1,3}\\.){3}[0-9]{1,3}\\])|(([\\w\\-]+\\.)+)([a-zA-Z]{2,4}))\$");
  static final passwordRegex = RegExp("^(?=.*\\d)(?=.*[a-z])(?=.*[A-Z]).{8,16}\$");
  static final lengthRegex = RegExp("^\\s*(?:\\S\\s*){8,16}\$");
  static final lowercaseRegex = RegExp("[a-z]");
  static final uppercaseRegex = RegExp("[A-Z]");
  static final numberRegex = RegExp("[0-9]");

  static String username(String username){
    if(!lengthRegex.hasMatch(username)){
      return "username must be 6 to 14 characters long";
    }
    return null;
  }

  static String email(String email){
    if(email.isEmpty){
      return "Enter an Email";
    }
    Iterable<Match> matches = emailChecker.allMatches(email);

    if(matches.length != 1){
      return "Invalid Email";
    }
    return null;
  }


  static String password(String pass){

    if(!lengthRegex.hasMatch(pass)) {
      return "Password must be 8 to 16 characters long";
    } else if(!lowercaseRegex.hasMatch(pass)) {
      return "must contain atleast one lowercase character";
    } else if(!uppercaseRegex.hasMatch(pass)) {
      return "must contain atleast one uppercase character";
    } else if(!numberRegex.hasMatch(pass)) {
      return "must contain atleast one number";
    } else if(!passwordRegex.hasMatch(pass)) {
      return "invalid password";
    }

    return null;
  }

}