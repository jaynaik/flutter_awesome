import 'package:shared_preferences/shared_preferences.dart';


class IoHandler {

  static String localPath ;
  static SharedPreferences prefs ;
  void _localPath() async {
    prefs = await SharedPreferences.getInstance();
    print("shared preferences accessible");
  }

  // Constructor
  IoHandler(){
    _localPath();
  }



  void setContentToPreference(String key, String content) {
    prefs.setString(key, content);
  }

  String getContentOfPreference(String key) {
    return prefs.getString(key);
  }

  bool getBoolFor(String key){
    if (prefs.containsKey(key)){
      return prefs.getBool(key);
    } else {
      return null;
    }
  }

  void setBoolFor(String key,bool value) {
    prefs.setBool(key, value);
  }


}


